# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository holds three environments stage,dev and prod configuration templates to use while accessing the terraform module.

### How do I get set up? ###

* Summary of set up : 
Download the github repository from using:
git clone https://Kirangudekar@bitbucket.org/Kirangudekar/module-version-demo.git

* Configuration : 
Open the folder in terraform IDE of your choice.
Run terraform init to download source repository
configure remote-backend to save tfstate on S3 bucket.

* Dependencies : 
setup the all variables required to and mentioned in variables.tf file in modules directory.

* How to run tests : 
terraform apply

* Deployment instructions : 
run the deployments for each version independenly like stage , dev or prod.
cross verify variable settings for each of these environments

### Who do I talk to? ###

* Repo owner 
