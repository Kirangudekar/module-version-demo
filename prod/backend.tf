terraform {
    backend "s3" {
    bucket = "remote-backend-kiran-g"
    key    = "demo-infra.tfstate"
    region = "us-east-2"
    dynamodb_table = "s3-demostate-lock"
  }
}
