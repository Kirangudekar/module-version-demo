module mydemo{
  source = "git::https://Kirangudekar@bitbucket.org/Kirangudekar/demo-module.git?ref=v0.0.2"
  vpc_id = "vpc-b8e212d3"
  cert  =  "arn:aws:acm:us-east-2:471443557195:certificate/d9fb6fdb-96f6-46f8-8b19-197d535fce91"
  Subnet1_CIDR  =  "172.31.64.0/18"
  Subnet2_CIDR  =  "172.31.48.0/20"
  instance_key  =  "terraform_launch_ec2"
  instancetype  =  "t2.micro"
  ami_ID        =  "ami-00dfe2c7ce89a450b"
}
provider aws{
region = "us-east-2"
}
